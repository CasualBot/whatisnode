/**
 * Created by shawn on 2016-02-24.
 */
var TwitterBot = require('twit');
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var Bot = new TwitterBot({
  consumer_key: '',
  consumer_secret: '',
  access_token: '',
  access_token_secret: ''
});

//Words, Hashtags, whatever to be tracked
var trackWords = ['#BoldLife', '#BoldCommerce', '#Bold'];

//This will turn the bot on and emit the tween out
Bot.stream('statuses/filter', { track: trackWords}).on('tweet', function(tweet){
  //Pass the tween to the front end
  io.emit('chat message', tweet);
});
  /**
 * Server index as our root path
 */
app.get('/', function(req, res) {
  res.sendFile(__dirname + '/index.html')
});

/**
 * Listen for when a user connects
 */
io.on('connection', function(socket) {
  console.log('A user connected!');

  socket.on('disconnect', function(){
    console.log('A user disconnected :(');
  });
});

/**
 * Create our server port listener
 */
http.listen('9100', function() {
  console.log("Listening on *:9100")
});

